(ns ncc.analyzer.fake-analyzer
  (:require [clojure.tools.analyzer :as a]
            [clojure.tools.analyzer.env :as env]))

;;; XXX: Code to throw away ...

;;; I show how to instantiate the (abstract) tools.analyzer
;;; using existing functions from the JVM env.

;;; Note in particular the use of the existing macroexpand functionality
;;; (which is OK I guess as long as there is no need for a bootstrap)

(defn fake-macroexpand-1 [form env]
  (macroexpand-1 form))

(defn create-var [sym env]
  (println "[create-var] sym = " sym)
  nil)

(defn empty-env []
  (atom {:namespaces {'user {:mappings {'+ #'+}
                             :aliases {}
                             :ns 'user}}}))


(defn analyze
  ([form] (analyze form {}))
  ([form env] (binding [a/macroexpand-1 fake-macroexpand-1
                        a/parse a/-parse
                        a/var? var?
                        a/create-var create-var]
                (env/with-env (empty-env)
                  (a/analyze form env)))))
